from django.db import models
from django.utils.translation import ugettext_lazy as _

from information.choices import TYPE_GENDER, TYPE_DOCUMENT


class Department(models.Model):
    name = models.CharField(max_length=255, verbose_name=_('Departamento'))

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = _('Departamentos')
        verbose_name = _('Departamento')


class Municipality(models.Model):
    department = models.ForeignKey(Department, on_delete=models.CASCADE, verbose_name=_('Departamento'))
    name = models.CharField(max_length=255, verbose_name=_('Municipio'))

    def __str__(self):
        return self.name + ' - ' + self.department.name

    class Meta:
        verbose_name_plural = _('Municipios')
        verbose_name = _('Municipio')


class InformationUser(models.Model):
    first_name = models.CharField(max_length=255, verbose_name='Nombre (s)')
    last_name = models.CharField(max_length=255, verbose_name='Apellido (s)')
    type_document = models.CharField(choices=TYPE_DOCUMENT, max_length=255, default='cc',
                                     null=True, blank=True, verbose_name=_('Tipo de documento'))
    identification = models.CharField(max_length=255, verbose_name=_('Número de identificación'))
    gender = models.CharField(choices=TYPE_GENDER, max_length=255, default='male', verbose_name=_('Género'))
    municipality = models.ForeignKey(Municipality, on_delete=models.CASCADE, verbose_name=_('Municipio'))
    phone = models.CharField(max_length=255, verbose_name=_('Teléfono'), null=True, blank=True)
    observations = models.TextField(verbose_name=_('Observaciones'), null=True, blank=True)
    vote = models.ForeignKey(Municipality, on_delete=models.CASCADE, verbose_name=_('Donde vota?'),
                             null=True, blank=True, related_name='vote')

    def __str__(self):
        return self.first_name + ' ' + self.last_name

    class Meta:
        verbose_name_plural = _('Informacion de usuarios')
        verbose_name = _('Informacion de usuarios')

# DataBase Pedro

Versión Python: `^3.4`

Versión django: `3.0.1`


#### Configuración del proyecto

* Clone repositorio
```
$ git clone 
$ cd DBPedro
```

* Cree virtualenv
```
$ virtualenv -p python3 env
$ source env/bin/activated
```

* Instale requerimientos
```
pip3 install -r requeriments.txt
```

* Ejecutar
```
python3 manage.py runserve
```

#### Despliegue

* Instalar `mysqlclient`

* Agregar los datos de la Base de datos HOST en `DBPedro/settings.py`

* Cambiar DATABASES en `DBPedro/settings.py` por HOST

* Cambiar DEBUG = False en `DBPedro/settings.py`


Hecho con ♥ por [Jose Florez](https://www.facebook.com/joserodolfo.florezortiz)